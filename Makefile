# Makefile to build stack images
# author: jloos@maxdaten.io
#
REPO = quay.io/briends
APP = new-stack-template
VERSION := $(shell cat new-template.cabal | grep '^version:' | grep -oE '[[:digit:]]+.[[:digit:]]+.[[:digit:]]+')
BUILDTAG = builder
IMG = $(REPO)/$(APP)
BUILDIMG = $(IMG):$(BUILDTAG)
VERSION_BUILDIMG = $(IMG):$(VERSION)-$(BUILDTAG)
PROJECT_DIR = $(shell pwd)
LOCAL_STACK = $(shell stack path --global-stack-root)
STACK_BUILD_VOLUMES = \
	-v $(PROJECT_DIR)/.app/:/root/.local/ \
	-v $(LOCAL_STACK):/root/.stack/ \
	-v $(PROJECT_DIR)/.stack-work:/usr/src/app/.stack-work/ \

default: app-image

appDir:
	@mkdir -p ./.app

app-build: appDir
	docker build --file=build.Dockerfile -t $(BUILDIMG) .
	docker tag -f $(BUILDIMG) $(VERSION_BUILDIMG)
	docker run $(STACK_BUILD_VOLUMES) $(BUILDIMG) setup
	docker run $(STACK_BUILD_VOLUMES) $(BUILDIMG) test
	docker run --rm $(STACK_BUILD_VOLUMES) $(BUILDIMG) install

app-image: | app-build
	docker build -t $(IMG) --file=Dockerfile .
	docker tag -f $(IMG):latest $(IMG):$(VERSION)

publish-build: | app-build
	docker push $(BUILDIMG)
	docker push $(VERSION_BUILDIMG)

publish-image: | app-image
	docker push $(IMG):latest
	docker push $(IMG):$(VERSION)

publish: | publish-build publish-image

run: | app-image
	@echo "starting '$(APP)'"
	@docker run --rm --name $(APP) -ti $(IMG)

clean:
	rm -rf ./.app

.PHONY: app-build app-image publish publish-build publish-image clean run
