# FROM index.docker.io/haskell:7.8
FROM debian:jessie

RUN echo 'deb http://ppa.launchpad.net/hvr/ghc/ubuntu trusty main' > /etc/apt/sources.list.d/ghc.list && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F6F88286 && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    zlib1g \
    ca-certificates \
    libgmp10

ENV PATH=/usr/app/bin:$PATH

# mountable
VOLUME ["/usr/app/bin/", "/var/lib/app/", "/var/log/app"]

COPY .app/bin/ /usr/app/bin/

WORKDIR /var/lib/app
ENTRYPOINT ["new-template-exe"]

EXPOSE 3000
