# Haskell Stack Docker template

A simple project as a template for Haskell project using `stack` for building and
dependency managment.

## What it does

- Creates a build docker image which...
    - Mounts project local `.app` directory for final build results
    - Mounts project local `.stack-work` directory intermediate build results
    - Mounts global `.stack` directory for build artifacts and `GHC`
  (will be shared between host an docker daemon and between projects)
- Runs `setup` `test` & `install` in build image
- Creates a run docker image by coping executable from `.app/bin` into a Haskell compatible docker environment